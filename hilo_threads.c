#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

 void *hilos(void *t)
 {
    int i;
    long HiloID;


    HiloID = (long)t;
    printf("\n\n ...Hilo numero %ld INICIANDO...\n",HiloID);

    printf("\n\n ...Hilo numero: %ld FINALIZANDO... \n",HiloID);
    pthread_exit((void*) t);
 }

 int noHilos;

 void numeroH (){
    printf("\n   ~  *Posix Hilos en C*  ~     ");
    printf("\n   ~  *Yessica Martinez Cruz*  ~        |");
    printf("\n\n\n  ... Ingrese la cantidad de hios: ");
    scanf ("%d",&noHilos);
 }

 int main (int argc, char *argv[])
 {
    numeroH();
    pthread_t thread[noHilos];
    pthread_attr_t at;
    int r;
    long t;
    void *st;


    pthread_attr_init(&at);
    pthread_attr_setdetachstate(&at, PTHREAD_CREATE_JOINABLE);

    for(t=0; t<noHilos /*t<NUM_HILOS*/; t++) {
       printf("\n\n Creando hilo numero: %ld\n", t);
       r = pthread_create(&thread[t], &at, hilos, (void *)t);
       if (r) {
          printf("\n> >E R R O R;  %d\n", r);
          exit(-1);
          }
       }

    pthread_attr_destroy(&at);
    for(t=0; t<noHilos/*t<NUM_HILOS*/; t++) {
       r = pthread_join(thread[t], &st);
       if (r) {
          printf("\nERROR; %d\n", r);
          exit(-1);
          }
       printf("\n\n HILO COMPLETADO...\n",t);
       }

 printf("\n\n\n �POSIX:Fin de programa�. ...SALIENDO...\n\n");
 pthread_exit(NULL);
 }
